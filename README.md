# Webinars - Build your own blog using the Microsoft tech stack

Detalii despre cele 8 sesiuni care se desfasoara in perioada 15 Februarie - 12 Aprilie 2022 gasiti mai jos

* [WebApi & REST](#webapi--rest)
* [Entity Framework](#entity-framework)
* [ASP.Net MVC](#aspnet-mvc)
* [Security](#security)
* [Async/Await](#asyncawait)
* [SOLID](#solid)
* [Unit Testing](#unit-testing)
* [Azure Demo](#azure)

## Resurse necesare pentru a rula/testa codul și pentru a rezolva temele:

    VS Code : https://code.visualstudio.com/download 
    .Net 5.0 Framework : https://dotnet.microsoft.com/en-us/download/dotnet/5.0
    Git : https://git-scm.com/download/win

## WebApi & REST

### Speakers
    Daniel Cotoară
    Alina Hamza
    Dragoș Dumitriu

* Prezentare [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/uploads/378ca41e5ce0dbdf0e2c0391a232e1ab/WebApi___REST.pdf)
* Temele disponibile [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/Teme-WebAPI&Rest-Webinar)
* Pasi necesari pentru rezolvarea temelor [aici](#pa%C8%99i-necesari-pentru-rezolvarea-temelor)

## Entity Framework

### Speakers
    Liviu Popovici
    Marius Brînzea

* Prezentare [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/uploads/34ae3592bc2e4103c041db52fa682c31/EntityFramework.pdf)
* Temele disponibile [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/Teme-EntityFramework-Webinar)
* Pasi necesari pentru rezolvarea temelor [aici](#pa%C8%99i-necesari-pentru-rezolvarea-temelor)

### Comenzi folosite pe durata prezentarii
```console
# Instalare pachete nuget. 
# Parametrul version e necesar pentru ca ultima versiune a acestor pachete sunt compatibile doar cu proiecte .Net 6
dotnet add package Microsoft.EntityFrameworkCore.InMemory --version 5.0.14
dotnet add package AutoMapper --version 11.0.1
dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection --version 11.0.0
dotnet add package Microsoft.EntityFrameworkCore.Sqlite --version 5.0.14

# Instalare suport entity framework pentru a rula comenzi din terminal
dotnet tool install --global dotnet-ef

# Adauga suport pt code-first migration
dotnet add package Microsoft.EntityFrameworkCore.Design --version 5.0.14

# Creeaza baza de data initiala
dotnet ef migrations add InitialCreate

# Executa migrarea pentru actualizare a bazei de date cu structura de date (tabele) conform claselor definite in proiect
# Pentru a migra modificarile ulterioare (noile DBSet-uri definite in cod) se ruleaza tot aceasta comanda
dotnet ef database update
```

#### Resetarea migrarilor deja efectuare si restaurarea structurii bazei de date la forma ei initiala (fara date)
```console
dotnet ef database update 0
dotnet ef database update
```
## ASP.Net MVC

### Speakers
    Dragoș Dumitriu
    Petru Dolhescu

* Prezentare [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/uploads/1fa9c80e834bf6550bbe9ed07e138458/ASP.NET_MVC.pdf)
* Temele disponibile [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/Teme-ASP.Net-MVC)
* Pasi necesari pentru rezolvarea temelor [aici](#pa%C8%99i-necesari-pentru-rezolvarea-temelor)

### Comenzi folosite pe durata prezentarii
```console
# Instalare pachete nuget. 
dotnet add package Microsoft.AspNetCore.Http --version 2.2.2
```

## Security

### Speakers
    Rares Macovei
    Andrei Robu

* Prezentare [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/uploads/cdd77a2f5813cdf6f65c0c84d7e7d7ac/Security.pdf)
* Temele disponibile [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/Teme-Security)
* Pasi necesari pentru rezolvarea temelor [aici](#pa%C8%99i-necesari-pentru-rezolvarea-temelor)

### Comenzi folosite pe durata prezentarii
```console
# Instalare pachete nuget. 
dotnet add package Microsoft.AspNetCore.Identity.EntityFrameworkCore --version 5.0.14

# Creeaza tabelele specifice Microsoft Identity
dotnet ef migrations add AddIdentity
dotnet ef database update


# SQLite Extension for VS Code
https://marketplace.visualstudio.com/items?itemName=alexcvzz.vscode-sqlite

#Note: Aplicatia va crea automat la pornire userul: 'Administrator@email.com' cu user role de 'Admin'. Parola folosita va fi cea specificata in Startup.cs in metoda de CreateRoles.
```

## Async/Await

### Speakers
    Daniel Cotoară
    Liviu Murarașu

* Prezentare [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/uploads/5701793c071196b56bd6736257d78258/Async_Await.pdf)
* Temele disponibile [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/Teme-Async_Await)
* Pasi necesari pentru rezolvarea temelor [aici](#pa%C8%99i-necesari-pentru-rezolvarea-temelor)

### Comenzi folosite pe durata prezentarii
```console
# Update la DataBase. A fost adaugat tabelul de blog post views.
dotnet ef database update
```

## SOLID

### Speakers
    Marius Brînzea
    Daniel Cotoară

* Prezentare [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/uploads/cba063c1eca5f0c61c8a0f015eb817c6/SOLID_Principles.pdf)
* Temele disponibile [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/Teme-SOLID)
* Pasi necesari pentru rezolvarea temelor [aici](#pa%C8%99i-necesari-pentru-rezolvarea-temelor)

### Extensions
```console
# Live share Extension for VS Code
https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare-pack

```

## Unit Testing

### Speakers
	Georgiana Bobric
	Alina Hamza

* Prezentare [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/uploads/e9235da0d59ae3ce482e6a84af645b53/UnitTesting.pdf)
* Temele disponibile [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/Teme-UnitTesting)
* Pasi necesari pentru rezolvarea temelor [aici](#pa%C8%99i-necesari-pentru-rezolvarea-temelor)

### Comenzi folosite pe durata prezentarii
```console
>> dotnet new xunit -n hello-blog-api-tests
>> dotnet "add" "[...]hello-blog-api\hello-blog-api-test.csproj" "package" "xunit"
>> dotnet "add" "[...]hello-blog-api\hello-blog-api-test.csproj" "package" "xunit.runner.visulastudio"
>> dotnet "add" "[...]hello-blog-api\hello-blog-api-test.csproj" "package" "moq"
```
### Resurse
```Console
https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-best-practices
https://docs.microsoft.com/en-us/shows/visual-studio-toolbox/unit-testing-moq-framework
```

## Azure

### Speakers
	Mihai Darie
	Marius Brinzea

* Prezentare [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/uploads/1895d4914305cd96c236c2fbedee957f/AzureDemo.pdf)
* Temele disponibile [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/Tema-AzureDemo)
* Pasi necesari pentru rezolvarea temelor [aici](#pa%C8%99i-necesari-pentru-rezolvarea-temelor)

### VS Code Extensions
```console
# Use VsCode Extensions area
Azure Account
Azure Resources
Azure App Service
Azure API Management
```

### Comenzi folosite pe durata prezentarii
```console
>> dotnet publish -c Debug -o ./publish
```

### Resurse
* https://portal.azure.com/
  * https://portal.azure.com/#create/Microsoft.WebSite
  * https://portal.azure.com/#create/Microsoft.ApiManagement
  * https://portal.azure.com/#blade/HubsExtension/BrowseResource/resourceType/Microsoft.Sql%2Fservers%2Fdatabases
    * https://portal.azure.com/#create/Microsoft.SQLDatabase 
    * dupa creare, se poate accesa connection string din [SQL_Resource]/Settings/Connection strings; acesta poate fi utilizat ulterior in proiectul de API pentru utilizarea acestei DB
    * https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-strings#aspnet-core
* Daca resursa/aplicatia voastra are URL: https://livedemo-webinar.azurewebsites.net/
  * Kudu panel (**scm** e adaugat ca subdomeniu inainte de azurewebsites.net): https://livedemo-webinar.scm.azurewebsites.net/
  * Logs: [kudu_url]/api/logstream
  * Generate ssh_key: [kudu_url]/api/sshkey?ensurePublicKey=1

## Pași necesari pentru rezolvarea temelor 

 1) Creare unui cont pe [gitlab](https://docs.gitlab.com/ee/gitlab-basics/index.html).

 2) Fork la [repository](https://gitlab.com/csv-webinars/hello-blog-api) utilizănd [pașii](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html).

 3) [Clonare](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) a repository-ului personal după fork.

 4) [Crearea](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#create-a-branch) unui branch (Opțional).

 5) Rezolvarea temei/temelor.

 6) [Commit](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#add-and-commit-local-changes) la modificări.

 7) [Push](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#send-changes-to-gitlabcom) la modificări în repository-ul personal.

 8) [Crearea](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#create-an-issue) unui Issue. Completați cămpurile după cum urmează:

        a) Title = Tema + Numele webinar-ului + Link-ul către repository-ul personal care a fost creat după fork. 
            Exemplu -> Tema WebApi&Rest : Link https://gitlab.com/csv-webinars/hello-blog-api.git
        b) Type = Issue
        c) Description = O descriere a ce s-a rezolvat în respectiva temă. Exemplu -> Din cele 3 teme am rezolvat complet 2 dintre ele (Tema_1 si Tema_2) și am încercat să fac ceva și la Tema_3
        d) Assignees = Cel puțin o persoană dintre cei care au ținut respectivul webinar (se pot vedea în secțiunea : Speakers)
        d) Restul câmpurilor se pot lăsa cu valorile default.

## Instrucțiuni folositoare pentru lucrul cu Git
- [Learn git branching](https://learngitbranching.js.org/)

## Instrucțiuni folositoare în VS Code Terminal
- Build la proiect : dotnet build
- Start la proiect : dotnet run 
    - Url-ul unde va fi hostată aplicația va aparea în Terminal. Acesta poate fi vizibil și în fișierul : launchSettings.json
- Alte [instrucțiuni](https://git-scm.com/docs/git#_git_commands) utile

## Pentru orice probleme va rugăm sa faceți un Issue urmănd pașii de mai sus cu precizarea de a schimba: 

    a) Title = Problema
    b) Type = Incident
    c) Description = Descrierea problemei și opțional atașare de imagini dacă este nevoie.
