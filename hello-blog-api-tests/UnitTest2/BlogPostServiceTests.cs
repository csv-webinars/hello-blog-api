using System;
using Xunit;
using hello_blog_api.Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace hello_blog_api_tests.UnitTest2
{
    public class BlogPostServiceTests: IDisposable
    { 
        private readonly BlogDbContext _context;

        // Add common logic that is used in each test
        public BlogPostServiceTests()
        {
            // create/open the test DB 
             _context = new BlogDbContext(new DbContextOptions<BlogDbContext>(), "blogging-test.db");
             _context.Database.OpenConnection();
             _context.Database.EnsureCreated();
            // seed the DB
             _context.BlogPosts.Add(
                new BlogPost {
                    Content = "content1", 
                    Label = "label1", 
                    Title = "title1"
                    });
            _context.SaveChanges();
        }
        
        public void Dispose()
        {
            _context.BlogPosts.RemoveRange(_context.BlogPosts);
            _context.SaveChanges();
            _context.Database.CloseConnection();
            _context.Dispose();
        }


        [Fact(DisplayName = "Test Create blog using test DB")]
        public async void CreateBlogPost_Using_Service_And_TestDb_Success()
        {
            // Arrange
            var service = new BlogPostService(_context);
            
            var blogPostRequest = new BlogPost
            {
                Content = "content2", 
                Label = "label2", 
                Title = "title2"
            };

            // Act
            BlogPost blogPostResult = await service.CreateBlogPostAsync(blogPostRequest);

            // Assert
            Assert.NotNull(blogPostResult);
            Assert.Equal(blogPostRequest.Content, blogPostResult.Content);
            Assert.Equal(blogPostRequest.Title, blogPostResult.Title);
            Assert.Equal(blogPostRequest.Label, blogPostResult.Label);
        }

    }
}
