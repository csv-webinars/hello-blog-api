using Xunit;
using hello_blog_api.Controllers;
using hello_blog_api.Repository;
using AutoMapper;
using hello_blog_api.Models;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace hello_blog_api_tests.UnitTest2
{
    public class BlogPostControllerTests : IDisposable
    {
        private readonly IMapper _mapper;

        private readonly IBlogPostService _service;

        private readonly BlogPostController _controller;

        public BlogPostControllerTests()
        {
            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });
            _mapper = mappingConfig.CreateMapper();
            _service = new BlogPostServiceFake();
            _controller = new BlogPostController(_service, _mapper);
        }

        public void Dispose()
        {
            _controller.Dispose();
        }

        [Fact]
        public async void CreateBlogPost_UsingServiceFake_Success()
        {
            // Arrange
            var blogPostModelCreate = new BlogPostModelCreate
            {
                Content = "content1",
                Label = "label1",
                Title = "title1"
            };

            // Act
            IActionResult responseAsync = await _controller.CreateBlogPost(blogPostModelCreate);

            // Assert
            var result = (CreatedAtActionResult)responseAsync;
            int? statusCode = result.StatusCode;
            Assert.Equal((int)HttpStatusCode.Created, statusCode);
            var blogPostResult = (BlogPostModel)result.Value;
            Assert.Equal(blogPostModelCreate.Content, blogPostResult.Content);
            Assert.Equal(blogPostModelCreate.Title, blogPostResult.Title);
            Assert.Equal(blogPostModelCreate.Label, blogPostResult.Label);
        }

    }

    public class BlogPostServiceFake : IBlogPostService
    {
        public async Task<BlogPost> CreateBlogPostAsync(BlogPost blogPost)
        {
            blogPost.Id = 1;
            await Task.CompletedTask;
            return blogPost;
        }

        public async Task<List<BlogPost>> GetAllBlogPostsAsync()
        {
            List<BlogPost> blogPosts = new List<BlogPost> { };
            await Task.CompletedTask;
            return blogPosts;
        }

        public async Task<BlogPost> GetBlogPostByIdAsync(int id)
        {
            BlogPost blogPost = new BlogPost { Id = 2, Content = "fake content", Label = "fake label", Title = "fake title" };
            await Task.CompletedTask;
            return blogPost;
        }

        public Task<bool> Update(BlogPostModel blogPostModel)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int blogPostId)
        {
            throw new NotImplementedException();
        }
    }
}
