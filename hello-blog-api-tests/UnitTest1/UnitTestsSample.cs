using System;
using System.Threading.Tasks;
using Xunit;

namespace hello_blog_api_tests
{
    public class UnitTest1: IDisposable
    {
        public UnitTest1()
        {
            // executes before each test as setup
        }

        [Fact]
        public void Test_SumOfTwoNumbers_Success()
        {
            // Arrange
            var a = 1;
            var b = 2;

            // Act
            var sum = a + b;

            // Assert
            Assert.Equal(3, sum);
        }

        [Fact(DisplayName = "Description for the test")]
        [Trait("Author", "FII webinar")]
        public void Test_DisplayName_Trait()
        {        

        }


        [Fact(Timeout = 3600)]
        public async Task Test_Timeout_OneMinute()
        {
            await Task.CompletedTask;
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void Test_TheoryBoolean(bool param)
        {
            // Arrange
            bool isTrue = true;

            // Act
            bool result = param && isTrue;

            // Assert
            Assert.Equal(param, result);
        }

        [Theory]
        [InlineData(1, 3)]
        [InlineData(2, 0)]
        public void Test_SumOfTwoNumbers_Fail(int param1, int param2)
        {
            // Arrange
            int a = param1;
            int b = param2;

            // Act
            int sum = a + b;

            // Assert
            Assert.NotEqual(3, sum);
        }

        [Fact(Skip = "The reason why we skip this test")]
        public void Test_Skipped()
        {

        }

        public void Dispose()
        {
            // code to execute after each test as teardown, to clear resources
        }
    }
}
