using System.Net;
using Xunit;
using hello_blog_api.Controllers;
using hello_blog_api.Repository;
using Moq;
using AutoMapper;
using hello_blog_api.Models;
using Microsoft.AspNetCore.Mvc;
namespace hello_blog_api_tests.UnitTest3
{
    public class BlogPostControllerTests
    {
        private readonly IMapper _mapper;

        public BlogPostControllerTests()
        {
            if (_mapper == null)
            {
                var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });
                _mapper = mappingConfig.CreateMapper();
            }
        }

        [Fact]
        public async void CreateBlogPost_UsingServiceMock_Success()
        {
            // Arrange
            var blogPostServiceMock = new Mock<IBlogPostService>();
            var blogPostResponse = new BlogPost{
                Id = 1, 
                Content = "content2", 
                Label = "label2", 
                Title = "title2"
            };
            blogPostServiceMock.Setup(bp => bp.CreateBlogPostAsync(It.IsAny<BlogPost>())).ReturnsAsync(blogPostResponse);

            var controller = new BlogPostController(blogPostServiceMock.Object, _mapper);
            var blogPostModelCreate = new BlogPostModelCreate{Content="content1", Label="label1", Title="title1"};

            // Act
            IActionResult responseAsync = await controller.CreateBlogPost(blogPostModelCreate);

            // Assert
            var result = (CreatedAtActionResult)responseAsync;
            int? statusCode = result.StatusCode;
            Assert.Equal((int)HttpStatusCode.Created, statusCode);
            var blogPostResult = (BlogPostModel) result.Value;
            Assert.Equal(blogPostResponse.Content, blogPostResult.Content);
            Assert.Equal(blogPostResponse.Title, blogPostResult.Title);
            Assert.Equal(blogPostResponse.Label, blogPostResult.Label);
        }
    }
}
