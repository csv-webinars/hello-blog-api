using Microsoft.AspNetCore.Mvc;
using hello_blog_api.Models;
using System.Net;
using hello_blog_api.Repository;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;

namespace hello_blog_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BlogViewsController : Controller
    {
        private readonly BlogDbContext _dbContext;
        private readonly IMapper _mapper;

        public BlogViewsController(BlogDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        ///<summary>
        ///Gets number of views of our blog.
        ///Endpoint url: api/v1/BlogViews
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetNumberOfViews()
        {
            try
            {
                var views = await _dbContext.BlogViews.FirstOrDefaultAsync();
                if (views?.Id < 1)
                    return StatusCode((int)HttpStatusCode.NotFound);

                return Ok(new BlogViewModel{Views = views.Views});
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Add or update blog number of views.
        ///If there is nothing in the table we will create a new row first
        ///If something already exists we will update that value
        ///In this case we use HttpPut verb to update or add. There is nothing wrong with this approach
        ///Endpoint url: api/v1/BlogViews
        ///</summary>
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpPut]
        public async Task<IActionResult> AddOrUpdate()
        {
            try
            {
                var blogViews = await _dbContext.BlogViews.ToListAsync();

                if (blogViews?.Count > 0)
                {
                    var blogpostView = blogViews.FirstOrDefault();
                    var numberOfViews = blogpostView.Views + 1;
                    blogpostView.Views = numberOfViews;
                    _dbContext.BlogViews.Update(blogpostView);
                }
                else
                {
                    var blogPostView = new BlogView { Views = 1 };
                    await _dbContext.BlogViews.AddAsync(blogPostView);
                }

                await _dbContext.SaveChangesAsync();

                return Ok();
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}