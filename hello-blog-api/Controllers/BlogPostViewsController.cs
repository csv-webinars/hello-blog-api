using Microsoft.AspNetCore.Mvc;
using hello_blog_api.Models;
using System.Net;
using hello_blog_api.Repository;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;

namespace hello_blog_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BlogPostViewsController : Controller
    {
        private readonly BlogDbContext _dbContext;
        private readonly IMapper _mapper;

        public BlogPostViewsController(BlogDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        ///<summary>
        ///Gets number of views of our blog.
        ///Endpoint url: api/v1/BlogPostViews
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("{blogPostId}")]
        public async Task<IActionResult> GetNumberOfViews([FromRoute] int blogPostId)
        {
            try
            {
                var blogPostViews = await _dbContext.BlogPostViews.Where(x => x.BlogPostId == blogPostId)
                                                                  .Select(blogPost => _mapper.Map<BlogPostViewModel>(blogPost))
                                                                  .FirstOrDefaultAsync();
                if (blogPostViews?.Id < 1)
                    return StatusCode((int)HttpStatusCode.NotFound);

                return Ok(blogPostViews);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Add or update blog post number of views.
        ///If there is nothing in the table we will create a new row first
        ///If something already exists we will update that value
        ///In this case we use HttpPut verb to update or add. There is nothing wrong with this approach
        ///Endpoint url: api/v1/BlogPostViews
        ///</summary>
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpPut]
        [Route("{blogPostId}")]
        public async Task<IActionResult> AddOrUpdate([FromRoute] int blogPostId)
        {
            try
            {
                var blogPostViews = await _dbContext.BlogPostViews.FirstOrDefaultAsync(x => x.BlogPostId == blogPostId);

                if (blogPostViews != null &&  blogPostViews.Id > 0)
                {
                    var numberOfViews = blogPostViews.Views + 1;
                    blogPostViews.Views = numberOfViews;
                    _dbContext.BlogPostViews.Update(blogPostViews);
                }
                else
                {
                    var blogPostView = new BlogPostView { Views = 1, BlogPostId = blogPostId };
                    await _dbContext.BlogPostViews.AddAsync(blogPostView);
                }

                await _dbContext.SaveChangesAsync();

                return Ok();
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}