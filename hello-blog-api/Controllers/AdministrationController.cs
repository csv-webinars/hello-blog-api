using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using hello_blog_api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace hello_blog_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize(Roles ="Admin")]
    public class AdministrationController : Controller
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUser> userManager;

        public AdministrationController(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
        }

        [HttpPost("role")]
        public async Task<IActionResult> CreateRole([FromBody] CreateRoleModel roleModel)
        {
            if (ModelState.IsValid)
            {
                var identityRole = new IdentityRole
                {
                    Name = roleModel.RoleName
                };

                var result = await roleManager.CreateAsync(identityRole);
                if (result.Succeeded)
                {
                    return Ok();
                }

                foreach (var error in result.Errors)
                {
                    var errors = new StringBuilder();
                    errors.AppendLine($"{error.Description}\n");
                    return BadRequest(errors.ToString());
                }
            }

            return BadRequest();
        }

        [HttpGet("role")]
        public IActionResult GetRoles()
        {
            var roles = roleManager.Roles;
            return Ok(roles);
        }

        [HttpPost("role/{roleId}/user")]
        public async Task<ActionResult> AddUsersToRole(string userName, string roleId)
        {
            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(roleId))
            {
                return BadRequest("Params can not be null or white space!");
            }

            var role = await roleManager.FindByIdAsync(roleId);
            if (role == null)
            {
                return BadRequest($"Cound not find a role with the id:{roleId}");
            }

            var user = await userManager.FindByNameAsync(userName);
            if (user == null)
            {
                return BadRequest($"Cound not find a user with the UserName:{userName}");
            }

            if(await userManager.IsInRoleAsync(user, role.Name))
            {
                return BadRequest($"User:{user.UserName} is already added to role:{role.Name}");
            }

            var result = await userManager.AddToRoleAsync(user, role.Name);
            if (result.Succeeded)
            {
                return Ok($"User:{user.UserName} was added to role:{role.Name}");
            }

            var errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.AppendLine($"{error.Description}\n");
            }

            return BadRequest(errors.ToString());
        }
    }
}