using Microsoft.AspNetCore.Mvc;
using hello_blog_api.Models;
using System.Net;
using hello_blog_api.Repository;
using AutoMapper;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Linq;


namespace hello_blog_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BlogPostController : Controller
    {
        private readonly IMapper _mapper;

        private readonly IBlogPostService _blogPostService;

        public BlogPostController(IBlogPostService service, IMapper mapper)
        {
            _blogPostService = service;
            _mapper = mapper;
        }

        ///<summary>
        ///Creates a blog using the payload information.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [Authorize]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created, Type = typeof(BlogPostModel))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateBlogPost([FromBody] BlogPostModelCreate blogPost)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Model is not valid!");
                }

                BlogPost entity = _mapper.Map<BlogPost>(blogPost);

                var entity2 = await _blogPostService.CreateBlogPostAsync(entity);

                if (entity2 == null)
                {
                    return StatusCode((int)HttpStatusCode.InternalServerError);
                }

                BlogPostModel blogPostModel = _mapper.Map<BlogPostModel>(entity2);
                return CreatedAtAction(nameof(CreateBlogPost), blogPostModel);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a list of all blogs.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(BlogPostModel))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetAllBlogPostsAsync()
        {
            try
            {
                var blogPosts = await _blogPostService.GetAllBlogPostsAsync();
                var blogPostsModels = blogPosts.Select(blogPost => _mapper.Map<BlogPostModel>(blogPost)).ToList();

                return Ok(blogPosts);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a blog based on the blogId.
        ///Endpoint url: api/v1/BlogPost/{blogId}
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(BlogPostModel))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("{blogPostId}")]
        public async Task<IActionResult> GetBlogPostById([FromRoute] int blogPostId)
        {
            try
            {
                BlogPost blogPost = await _blogPostService.GetBlogPostByIdAsync(blogPostId);

                if (blogPost == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                var blogPostModel = _mapper.Map<BlogPostModel>(blogPost);
                return Ok(blogPostModel);
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Delete a blog based on the blogId.
        ///Endpoint url: api/v1/BlogPost/{blogId}
        ///</summary>
        [HttpDelete]
        [Authorize]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("{blogPostId}")]
        public async Task<IActionResult> Delete([FromRoute] int blogPostId)
        {
            try
            {
                var deleted = await _blogPostService.Delete(blogPostId);
                if (!deleted)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }

                return Ok();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Update a blog based on the blogId.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpPut]
        [Authorize]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Put([FromBody] BlogPostModel blogPostModel)
        {
            try
            {
                var updated = await _blogPostService.Update(blogPostModel);
                if (!updated)
                {
                    return StatusCode((int)HttpStatusCode.InternalServerError);
                }

                return Ok();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}