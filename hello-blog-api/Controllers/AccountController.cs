using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hello_blog_api.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace hello_blog_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly RoleManager<IdentityRole> roleManager;

        public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
        }
        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] RegisterUserModel userModel)
        {
            if (ModelState.IsValid)
            {
                var user = new IdentityUser
                {
                    UserName = userModel.Email,
                    Email = userModel.Email
                };

                var result = await userManager.CreateAsync(user, userModel.Password);

                if (result.Succeeded)
                {
                    var userRole = roleManager.Roles.Where(r => r.Name.Equals("User")).ToList();
                    if (userRole != null && userRole.Count > 0)
                    {
                        var addToRoleResult = await userManager.AddToRoleAsync(user, "User");
                        if (!addToRoleResult.Succeeded)
                        {
                            await userManager.DeleteAsync(user);
                            return BadRequest();
                        }
                    }
                    else
                    {
                        await roleManager.CreateAsync(new IdentityRole { Name = "User" });
                        var addToRoleResult = await userManager.AddToRoleAsync(user, "User");
                        if (!addToRoleResult.Succeeded)
                        {
                            await userManager.DeleteAsync(user);
                            return BadRequest();
                        }
                    }

                    return Ok("User was created!");
                }
                else
                {
                    var errors = new StringBuilder();
                    foreach (var error in result.Errors)
                    {
                        errors.AppendLine($"{error.Description}\n");
                    }

                    return BadRequest(errors.ToString());
                }
            }

            return BadRequest();
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginUserModel userModel)
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByEmailAsync(userModel.Email);
                if (user == null)
                {
                    return BadRequest("Invalid Login and/or password");
                }

                var passwordSignInResult = await signInManager.PasswordSignInAsync(user, userModel.Password, isPersistent: true, lockoutOnFailure: false);
                if (!passwordSignInResult.Succeeded)
                {
                    return BadRequest("Invalid Login and/or password");
                }

                var role = userManager.GetRolesAsync(user).Result.FirstOrDefault();

                return Ok(role);
            }

            return BadRequest("Invalid Login and/or password");
        }

        [HttpPost("Logout")]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();

            return Ok("You have been successfully logged out");
        }
    }
}