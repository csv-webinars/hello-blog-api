using System;

namespace hello_blog_api.Repository
{
    public class BlogPost : BaseEntity
    {
        public string Label { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }
    }
}