using System;

namespace hello_blog_api.Repository
{
    ///<summary>
    ///Each entity should inherit from this class.
    ///</summary>
    public class BaseEntity
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}