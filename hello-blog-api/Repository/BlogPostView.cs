namespace hello_blog_api.Repository
{
    public class BlogPostView : BaseEntity
    {
        public int BlogPostId { get; set; }
        public int Views { get; set; }
    }
}