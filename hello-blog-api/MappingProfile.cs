using AutoMapper;
using hello_blog_api.Models;
using hello_blog_api.Repository;

public class MappingProfile : Profile 
{
   public MappingProfile()
   {
      // Configure two-way mapping between desired models
      CreateMap<BlogPost, BlogPostModel>().ReverseMap();
      CreateMap<BlogPost, BlogPostModelCreate>().ReverseMap();
      CreateMap<BlogView, BlogViewModel>().ReverseMap();
      CreateMap<BlogPostView, BlogPostViewModel>().ReverseMap();
   }
}