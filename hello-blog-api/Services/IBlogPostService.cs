using System.Collections.Generic;
using System.Threading.Tasks;
using hello_blog_api.Models;
using hello_blog_api.Repository;

public interface IBlogPostService
{
    Task<BlogPost> CreateBlogPostAsync(BlogPost blogPost);

    Task<List<BlogPost>> GetAllBlogPostsAsync();

    Task<BlogPost> GetBlogPostByIdAsync(int id);
    Task<bool> Update(BlogPostModel blogPostModel);
    Task<bool> Delete(int blogPostId);
}