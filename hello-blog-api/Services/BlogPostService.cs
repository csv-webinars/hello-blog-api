using System.Collections.Generic;
using System.Threading.Tasks;
using hello_blog_api.Models;
using hello_blog_api.Repository;
using System.Linq;
using Microsoft.EntityFrameworkCore;

public class BlogPostService : IBlogPostService
{
    private readonly BlogDbContext _dbContext;
    public BlogPostService(BlogDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<BlogPost> CreateBlogPostAsync(BlogPost blogPost)
    {
        var createdEntity = await _dbContext.BlogPosts.AddAsync(blogPost);
        var savedItemsCount = await _dbContext.SaveChangesAsync();
        if (savedItemsCount != 1)
        {
            return null;
        }
        return createdEntity.Entity;
    }

    public async Task<List<BlogPost>> GetAllBlogPostsAsync()
    {
        // blocheaza Thread-ul pentru o anumita perioada de timp.
        // Daca este nevoie ar trebui utilizat Task.Delay
        // Thread.Sleep(5000);

        //await Task.Delay(5000);
        List<BlogPost> blogPosts = await _dbContext.BlogPosts.Select(blogPost => blogPost).ToListAsync();
        return blogPosts;

        // Mapare manuala
        // List<BlogPostModel> blogPosts = await _dbContext.BlogPosts
        //     .Select(x => new BlogPostModel(){ 
        //         Id = x.Id,
        //         Content = x.Content,
        //         Title = x.Title
        //         })
        //     .ToListAsync();

        // method syntax cu mapare automata cu automapper
        // List<BlogPostModel> blogPosts = await _dbContext.BlogPosts
        //     .Select(blogPost => _mapper.Map<BlogPostModel>(blogPost))
        //     .ToListAsync();

        // query syntax cu mapare automata cu automapper
        //List<BlogPostModel> blogPosts = await (
        //            from blogPost in _dbContext.BlogPosts
        //            select _mapper.Map<BlogPostModel>(blogPost))
        //            .ToListAsync();

        //var linqQuery = _dbContext.BlogPosts.Select(blogPost => _mapper.Map<BlogPostModel>(blogPost));
        // var linqQueryAsSQLCommand = linqQuery.ToQueryString();
    }

    public async Task<BlogPost> GetBlogPostByIdAsync(int id)
    {
        var blogPost = await _dbContext.BlogPosts.FirstOrDefaultAsync(bp => bp.Id == id);
        return blogPost;
    }

    public async Task<bool> Update(BlogPostModel blogPostModel)
    {
        var existingBlogPost = await _dbContext.BlogPosts.FirstOrDefaultAsync(x => x.Id == blogPostModel.Id);
        if (existingBlogPost == null)
        {
            return false;
        }

        existingBlogPost.Id = blogPostModel.Id;
        existingBlogPost.Title = blogPostModel.Title;
        existingBlogPost.Content = blogPostModel.Content;
        existingBlogPost.Label = blogPostModel.Label;

        _dbContext.BlogPosts.Update(existingBlogPost);
        
        return await _dbContext.SaveChangesAsync() > 0;
    }

    public async Task<bool> Delete(int blogPostId)
    {
        var blogPost = await _dbContext.BlogPosts.FirstOrDefaultAsync(x => x.Id == blogPostId);
        if (blogPost == null)
        {
            return false;
        }
        _dbContext.BlogPosts.Remove(blogPost);

        return await _dbContext.SaveChangesAsync() > 0;
    }
}