using System;

namespace hello_blog_api.Models
{
    public class BlogPostModel
    {
        public int Id { get; set; }

        public string Label { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}