using System;

namespace hello_blog_api.Models
{
    public class BlogPostViewModel
    {
        public int Id { get; set; }
        public int BlogPostId { get; set; }
        public int Views { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}