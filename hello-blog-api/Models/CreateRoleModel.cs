using System.ComponentModel.DataAnnotations;

namespace hello_blog_api.Models
{
    public class CreateRoleModel
    {
        [Required]
        public string RoleName { get; set; }
    }    
}