using System.ComponentModel.DataAnnotations;

namespace hello_blog_api.Models
{
    public class RegisterUserModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string  Password { get; set; }
    }
}
