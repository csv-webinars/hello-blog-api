using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace hello_blog_api.Middlewares
{
    public class CustomMiddleware
    {
        private readonly RequestDelegate _next;
        public CustomMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            context.Request.Headers.TryGetValue("User-Agent", out StringValues userAgent);

            if(!userAgent.Any(userAgent => userAgent.Contains("Postman", System.StringComparison.InvariantCultureIgnoreCase)))
            {
                await _next(context);
            }
        }
    }
}

public static class CustomMiddlewareExtensions
{
    public static IApplicationBuilder UseCustomMiddleware(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<hello_blog_api.Middlewares.CustomMiddleware>();
    }
}